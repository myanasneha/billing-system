import React, { useState ,useEffect} from 'react';
import '../styles/design.css';
import MyBillsComponent from './MyBillsComponent';
import moment from 'moment';

function NewBillComponent(props) {
  const [selectedItemValue, setSelectedItemValue] = useState('');
  const [quantity, setQuantity] = useState('');
  const [selectItemValidationMsg, setSelectItemValidationMsg] = useState('');
  const [totalItemsList, setTotalItemsList] = useState(props.itemsList);
  const [billItemsList, setBillItemsList] = useState([]);
  const [checkout, setCheckout] = useState(false);
  const [checkoutmsg, setCheckoutMsg] = useState("");

  useEffect(() => {
    setTotalItemsList(props.itemsList);
  }, [props.itemsList]); 
    
  function handleSubmitSelectedItem() {
    if(!selectedItemValue) {
      setSelectItemValidationMsg('Select Any Item');
    } else if(!quantity || quantity == 0) {
      setSelectItemValidationMsg('Quantity is Required');
    } else {
      let myBillsList = billItemsList;
      const priceOfTheSelectedItem = totalItemsList.filter(item => item.itemName == selectedItemValue)
      let addSelectItem = {};
      addSelectItem['itemName'] = selectedItemValue;
      addSelectItem['itemQuantity'] = quantity;
      addSelectItem['itemBill'] = parseInt(quantity) * priceOfTheSelectedItem[0].itemPrice;
      myBillsList.push(addSelectItem);
      setBillItemsList(myBillsList);
      setQuantity('');
      setSelectedItemValue('');
      setSelectItemValidationMsg('');
    }
    setCheckoutMsg("");
  }

  function totalPrice() {
    let sum = billItemsList.reduce(function(prev, current) {
      return prev + +current.itemBill
    }, 0);
    return sum
  }

  function handleCheckout() {
    if(billItemsList.length > 0){
      let checkoutBillsList = JSON.parse(localStorage.getItem('myBillsList')) || [];
      let billWithCost = {};
      billWithCost['billID'] = new Date().valueOf();
      billWithCost['itemsList'] = billItemsList;
      billWithCost['totalAmount'] = totalPrice();
      billWithCost['billDate'] = moment(new Date()).format("MMM DD, YYYY");
      checkoutBillsList.push(billWithCost);
      localStorage.setItem('myBillsList', JSON.stringify(checkoutBillsList));
      setBillItemsList([]);
      setCheckoutMsg("");
      billItemsList.length == 0 && setCheckout(true);
    } else {
      setCheckoutMsg("Please select any items");
    }
  }

  return (
    <div>
      <form>
        <fieldset>
          <h2>Select Item</h2>
          <label>
            <select type="text" className="form-control" placeholder="select item" onChange={(e) => setSelectedItemValue(e.target.value)} value={selectedItemValue}>
              <option value="select">Select any item</option>
              {totalItemsList && totalItemsList.length > 0 && totalItemsList.map((data, i) => <option key={i} value={data.itemName}>{data.itemName}</option>)}
            </select>
          </label>
          <label>
            <p>Quantity</p>
            <input type="number" name="quantity" value={quantity} onChange={(e) => setQuantity(e.target.value)} step="3"/>
          </label>
          <p style={{color: 'red'}}>{selectItemValidationMsg}</p>
          <button type="button" onClick={handleSubmitSelectedItem}>Submit</button>
        </fieldset>
      </form>
      <form>
        <fieldset>
          <h2>New Bill</h2>
          <table>
            <tr>
              <th>Item Name</th>
              <th>Item Quantity</th>
              <th>Price</th>
            </tr>
            {billItemsList && billItemsList.length > 0 && billItemsList.map((item, index) => 
              <tr key={index}>
                <td>{item.itemName}</td>
                <td>{item.itemQuantity}</td>
                <td>Rs. {item.itemBill}</td>
              </tr>
            )}
            {billItemsList.length < 1 &&
              <tr><td colspan="3">No data found</td></tr>
            }
            <tr ><td colspan="2">Total Items {billItemsList.length}</td><td >Amount Rs. {totalPrice()}</td></tr>
          </table>
          <button type="button" onClick={handleCheckout}><h2>Checkout</h2></button>
          <p style={{color: 'red'}}>{checkoutmsg}</p>
        </fieldset>
      </form>
      <MyBillsComponent checkout={checkout} myBillsList={JSON.parse(localStorage.getItem('myBillsList')) || []} />
    </div>
  )
}

export default NewBillComponent;
