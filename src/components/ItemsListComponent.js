import React from 'react';
import NewBillComponent from './NewBillComponent';
import '../styles/design.css';

class ItemsListComponent extends React.PureComponent {
  constructor(props) {
    super();
    this.state = {
      itemPrice: '',
      itemName: '',
      addItemErrorMsg: ''
    };
    this.handleAddNewItem = this.handleAddNewItem.bind(this);
  }

  handleAddNewItem() {
    if(!this.state.itemName.trim()) {
      this.setState({ addItemErrorMsg: "Item Name is Required" });
    } else if(!this.state.itemPrice) {
      this.setState({ addItemErrorMsg: "Item Price is Required" });
    } else {
      let itemsList = JSON.parse(localStorage.getItem('itemsList')) || [];
      let addNewItem = {};
      addNewItem['itemName'] = this.state.itemName;
      addNewItem['itemPrice'] = this.state.itemPrice;
      itemsList.push(addNewItem);
      localStorage.setItem('itemsList',JSON.stringify(itemsList));
      this.setState({ addItemErrorMsg: '', itemName: '', itemPrice: ''});
    }
  }

  render() {
    let totalItemsList = JSON.parse(localStorage.getItem('itemsList')) || [];
    return (
      <div className="wrapper">
        <form>
          <fieldset>
            <h2>Add Item</h2>
            <label>
              <p>Name*: </p>
              <input type="text" name="itemName" value={this.state.itemName} onChange={(e) => this.setState({ itemName: e.target.value })} />
            </label>
            <label>
              <p>Price*: </p>
              <input type="number" name="itemPrice" value={this.state.itemPrice} onChange={(e) => this.setState({ itemPrice: e.target.value })} step="1"/>
            </label>
            <p style={{color: 'red'}}>{this.state.addItemErrorMsg}</p>
            <button type="button" onClick={this.handleAddNewItem}>Submit</button>
          </fieldset>
        </form>
        <form>
          <fieldset>
            <h2>Items List</h2>
            <table>
              <tr>
                <th>Item Name</th>
                <th>Item Price</th>
              </tr>
              {totalItemsList && totalItemsList.length > 0 && totalItemsList.map((item, index) => 
                <tr key={index}>
                  <td>{item.itemName}</td>
                  <td>Rs. {item.itemPrice}</td>
                </tr>
              )}
              {totalItemsList.length < 1 &&
                <tr><td colspan="3">No data found</td></tr>
              }
            </table>
          </fieldset>
        </form>
        <NewBillComponent itemsList={totalItemsList} />
      </div>
    )
  }
}

export default ItemsListComponent;
