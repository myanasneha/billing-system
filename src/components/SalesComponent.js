import React, { useState, useEffect } from 'react';
import '../styles/design.css';
import moment from 'moment';

function SalesComponent(props) {
  const [billsList, setBillsList] = useState(props.billsList || []);
  const [todayBillCount, setTodayBillCount] = useState('');
  const [monthlyBillCount, setMonthlyBills] = useState('');
  const [yearlyBillCount, setYearlyBillCount] = useState('');

  useEffect(() => {
    setBillsList(props.billsList);
    getTodayBillsCount(props.billsList);
    getMonthlyBillsCount(props.billsList);
    getYearlyBillsCount(props.billsList);
  }, [props.billsList]);

  function getTodayBillsCount(billsList) {
    const listOfTodaysBills = billsList.filter(item => item.billDate == moment(new Date()).format("MMM DD, YYYY"))
    setTodayBillCount(listOfTodaysBills.length);
  }

  function getMonthlyBillsCount(billsList) {
    let newDate = new Date()
    let month = newDate.getMonth()+1;
    const listOfMonthlyBills = billsList.filter(item => parseInt(new Date(item.billDate).getMonth())+1 == month)
    setMonthlyBills(listOfMonthlyBills.length)
  }

  function getYearlyBillsCount(billsList) {
    let newDate = new Date()
    let year = newDate.getFullYear();
    const listOfYearlyBills = billsList.filter(item => new Date(item.billDate).getFullYear() == year)
    setYearlyBillCount(listOfYearlyBills.length)
  }

  return (
    <div>
      <fieldset>
        <h2>Sales</h2>
        <div className="sales" id="one">
          <p>Today {todayBillCount}</p> 
        </div>
        <div className="sales" id="two">
          <p>Monthly {monthlyBillCount}</p> 
        </div>
        <div className="sales" id="three">
          <p>Year {yearlyBillCount}</p> 
        </div>
      </fieldset>
    </div>
  )
}

export default SalesComponent;
