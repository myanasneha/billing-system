import React, { useState, useEffect } from 'react';
import '../styles/design.css';
import SalesComponent from './SalesComponent';

function MyBillsComponent(props) {
  const [billsList, setBillsList] = useState(props.myBillsList || []);

  useEffect(() => {
    setBillsList(props.myBillsList);
  }, [props.myBillsList]); 

  return (
    <div>
      <form>
        <fieldset>
          <h2>My Bills</h2>
          <table>
            <tr>
              <th>Bill ID</th>
              <th>Bill Date</th>
              <th>Amount</th>
            </tr>
            {billsList && billsList.length > 0 && billsList.map((item, index) => 
              <tr key={index}>
                <td>{item.billID}</td>
                <td>{item.billDate}</td>
                <td>Rs. {item.totalAmount}</td>
              </tr>
            )}
            {billsList.length < 1 &&
              <tr><td colspan="3">You Have No Bills</td></tr>
            }
          </table>
        </fieldset>
      </form>
      <SalesComponent billsList={billsList} />
    </div>
  )
}

export default MyBillsComponent;
